Rails.application.routes.draw do
  post 'exportables/pdf/:code', to: 'exportables#pdf_generator', defaults: { format: 'pdf' }
  resources :facturas, only: [:show]
end
