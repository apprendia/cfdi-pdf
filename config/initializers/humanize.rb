require 'net/http'
require 'json'
require_relative './sat'

module Humanize

 	UNIDADES_BASICAS	= ['cero', 'un', 'dos', 'tres', 'cuatro', 'cinco', 'seis', 'siete', 'ocho', 'nueve', 'diez', 'once', 'doce', 'trece', 'catorce', 'quince', 'diecisés', 'diecisiete', 'dieciocho', 'diecinueve', 'veinte']
  DECENAS 					= ['', 'diez', 'veinti', 'treinta', 'cuarenta', 'cincuenta', 'sesenta', 'setenta', 'ochenta', 'noventa']
  CENTENAS 					= ['', 'cien', 'doscientos', 'trescientos', 'cuatrocientos', 'quinientos', 'seiscientos', 'setecientos', 'ochocientos', 'novecientos']
  MILLARES 					= ['', 'mil', 'millon']

  def nombre_catalogo(clave)
    Sat.nombre_catalogo :c_moneda, clave
  end

  def humanize_divisa(code)
    num = self.to_i.abs
    o = ''
    if num.zero?
      o += 'cero '
    else
      sets = []
      i = 0
      while !num.zero?
        num, r = num.divmod(1000)
        sets << MILLARES[i] if !(r.zero? || i.zero?)

        subs = ''
        if !r.zero?
        	cen, r = r.divmod(100)
        	subs << CENTENAS[cen] + (cen == 1 && !r.zero? ? 'to' : '') + (r.zero? ? '' : ' ') unless cen.zero?

        	if !r.zero?
        		if r <= 20
        			subs << UNIDADES_BASICAS[r] if i.zero? or r > 1
        		else
        			dec, r = r.divmod(10)
        			subs << DECENAS[dec] + (dec > 2 && !r.zero? ? ' y ' : '')
        			subs << UNIDADES_BASICAS[r] unless r.zero?
        		end
        	end
        end
        sets << subs unless subs.empty?
        i = i.succ

      end
      o += sets.reverse.join(' ')
    end
    "#{o} #{self.to_f.round(2).to_s.split(/\./, 2).last[0, 2].ljust(2, '0')}/100 #{nombre_catalogo(code).split(' ').map(&:pluralize).join(' ')}".capitalize
  end

  def humanize
  	num = self.to_i.abs
    o = ''
    if num.zero?
      o += 'cero '
    else
      sets = []
      i = 0
      while !num.zero?
        num, r = num.divmod(1000)
        sets << MILLARES[i] if !(r.zero? || i.zero?)

        subs = ''
        if !r.zero?
        	cen, r = r.divmod(100)
        	subs << CENTENAS[cen] + (cen == 1 && !r.zero? ? 'to' : '') + (r.zero? ? '' : ' ') unless cen.zero?

        	if !r.zero?
        		if r <= 20
        			subs << (r == 1 ? 'uno' : UNIDADES_BASICAS[r]) if i.zero? or r > 1
        		else
        			dec, r = r.divmod(10)
        			subs << DECENAS[dec] + (dec > 2 && !r.zero? ? ' y ' : '')
        			subs << (r == 1 ? 'uno' : UNIDADES_BASICAS[r]) unless r.zero?
        		end
        	end
        end
        sets << subs unless subs.empty?
        i = i.succ

      end
      o += sets.reverse.join(' ')
    end
    o
  end
end

class Numeric
  include Humanize
end
