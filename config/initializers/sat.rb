require 'net/https'
require "open-uri"
require 'json'

class Sat
  class << self
    def nombre_catalogo(catalogo, clave)
      uri = URI("#{Rails.application.config.elastic_host}/#{catalogo.to_s.downcase}/_doc/#{clave}/_source?_source_include=name")
      response = nil

      Net::HTTP.start(uri.host, uri.port, use_ssl: uri.scheme == 'https') do |http|
        response = http.request(Net::HTTP::Get.new (uri)).body
      end

      Rails.cache.fetch [catalogo, clave] do
        JSON.parse(response)['name'] rescue ''
      end
    end

    def nombre_impuesto(clave)
      case clave
      when '001' then
        'ISR'
      when '002' then
        'IVA'
      when '003' then
        'IEPS'
      end
    end
  end
end