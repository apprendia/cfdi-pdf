module Basic
  module Complementos
    class Base
      class << self
        def render(context)
          begin
            new(context).render
          rescue => e
            raise e unless Rails.env.production?
          end
        end
      end

      def initialize(context = nil)
        @context = context
      end

      def render(*args)
        raise NotImplementedError
      end

      def pdf
        @context
      end

      private

      def method_missing(symbol, *args)
        return @context.send(symbol, *args) if @context.respond_to? symbol
        super
      end

      def respond_to_missing?
        super
      end

    end
  end
end