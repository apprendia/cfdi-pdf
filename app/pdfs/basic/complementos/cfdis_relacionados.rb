module Basic
  module Complementos
    class CfdisRelacionados < Base

      def render
        return nil unless cfdis_relacionados.any?

        pdf.make_table([row_title, *rows]) do
          rows(0..-1).borders = []
          rows(1..-1).padding_top = 0
          rows(1..-2).padding_bottom = 0
          row(0).font = 'Gotham Bold'
          row(0).size = 8.pt
        end
      end

      private

      def cfdis_relacionados
        @cfdis_relacionados ||= Array(
          instance_variable_get(:@context)
            .instance_variable_get(:@factura_xml)
            .instance_variable_get(:@table)[:CfdiRelacionados]
        )
      end

      def row_title
        [make_cell(content: 'CFDIs Relacionados')]
      end

      def rows
        body = []

        cfdis_relacionados.each do |obj|
          clave = obj[:TipoRelacion]

          body << [
            {
              content: "<strong><color rgb='888888'>#{Sat.nombre_catalogo('c_tiporelacion', clave.to_i)} (Clave #{clave}):</color></strong>",
              inline_format: true
            }
          ]
          body << [ Array(obj[:CfdiRelacionado]).map(&:UUID).join(', ') ]
        end

        body
      end



    end
  end
end