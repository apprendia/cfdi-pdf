module Basic
  module Complementos
    class Ine < Base

      def render
        pdf.make_table([row_title, row_details, *rows_entidades], width: pdf.bounds.width) do
          rows(0..-1).borders = []
          rows(1..-1).padding_top = 0
          row(0).font = 'Gotham Bold'
          row(0).size = 8.pt
        end if ine.present?
      end

      private

      def ine
        @ine ||= instance_variable_get(:@context)
                   .instance_variable_get(:@factura_xml)
                   .instance_variable_get(:@table)[:Complemento]
                   .instance_variable_get(:@table)[:INE]
      end

      def row_title
        [make_cell(content: 'Complemento INE')]
      end

      def row_details
        str = ''

        str << "Tipo de Proceso: #{ine[:TipoProceso] || ' - - - '}. "
        str << "Tipo de Comité: #{ine[:TipoComite] || ' - - - '}. "
        str << "Clave de Contabilidad: #{ine[:IdContabilidad] || ' - - - '}. "

        [make_cell(content: str)]
      end

      def rows_entidades
        Array(ine.Entidad).map do |entidad|
          str = ''

          str << if entidad[:ClaveEntidad]
                   "Entidad: (#{entidad[:ClaveEntidad]}). "
                 else
                   ' - - -. '
                 end

          str << "Ámbito: #{entidad[:Ambito] || ' - - - '}. "

          claves_contabilidad = entidad[:Contabilidad]&.map { |o| o[:IdContabilidad] }&.join(', ')
          str << "Claves de contabilidad: #{claves_contabilidad || ' - - - '}. "

          [make_cell(content: str)]
        end
      end


    end
  end
end