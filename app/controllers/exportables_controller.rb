require 'cgi'
require 'active_support/core_ext/hash' #from_xml
require 'nokogiri'

class ExportablesController < ApplicationController
  include ActionController::MimeResponds

  def pdf_generator
    result = PdfGenerationService.new(params).pdf

    if result.success?
      respond_to do |format|
        format.pdf { send_data result.data, disposition: 'attachment' }
      end
    else
      Rails.logger.debug(result.error)
      render nothing: true, status: 400
    end
  end


  private

  def response_error(msg = '')
    render json: {json: msg}, status: 400
  end
end
