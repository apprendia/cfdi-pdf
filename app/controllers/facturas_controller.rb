class FacturasController < ApplicationController
  include ActionController::MimeResponds

  def show
    return error_response(status: 404) if pdf_service_params.nil?

    respond_to do |format|
      format.pdf do
        pdf = PdfGenerationService.new(pdf_service_params).pdf

        if pdf.success?
          send_data pdf.data, disposition: 'inline', type: 'application/pdf', filename: "#{params[:id]}.pdf"
        else
          Rails.logger.debug(pdf.error)
          error_response(status: 500)
        end
      end

      format.xml do
        send_data pdf_service_params[:xml], disposition: 'inline', type: 'text/xml', filename: "#{params[:id]}.xml"
      end

      format.any { error_response(status: 400) }
    end
  end

  private

  def pdf_service_params
    @pdf_service_params ||= PdfGenerationService::Params.from_uuid(params[:id])
  end

  def error_response(status:)
    render nothing: true, status: status || 500
  end
end
