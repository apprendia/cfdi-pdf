class PdfGenerationService
  class Params

    class << self
      def from_uuid(uuid)
        factura_summary = FirestoreService.new.get_factura(uuid)
        factura_elastic = MC::ElasticFactura.find(uuid)

        obj = {
          template: factura_elastic.template.to_h,
          serie: factura_elastic.serie,
          folio: factura_elastic.folio
        }

        xml_io = open(factura_summary[:xmlUrl])

        { json: obj, xml: xml_io }

      rescue StandardError => e
        Rails.logger.error(e)
        nil
      end
    end

  end
end