class ServiceError
  attr_accessor :data, :error

  def initialize(data, error)
    @data = data
    @error = error
  end

  def success?
    false
  end
end