class PdfGenerationService
  def initialize(params)
    @params = params || {}
  end

  def pdf
    return ServiceError.new(nil, 'No template code provided') unless template_code.present?
    return ServiceError.new(nil, 'No CFDI provided') unless raw_xml.present? || raw_json.present?

    pdf = Prawn::Document.new(document_properties) { |pdf|
      pdf.instance_variable_set('@xml', raw_xml)
      pdf.instance_variable_set('@xml_doc', xml_doc)
      pdf.instance_variable_set('@factura_json', factura_json)
      pdf.instance_variable_set('@factura_xml', factura_xml&.Comprobante)
      pdf.instance_eval(view.read)
    }

    ServiceSuccess.new(pdf.render)
  end

  private

  def factura_xml
    return nil if raw_xml.blank?
    json = Hash.from_xml(xml_doc.to_s).to_json

    JSON.parse(json, object_class: OpenStruct).tap do |obj|
      timbre = obj.Comprobante&.Complemento&.TimbreFiscalDigital
      obj.Comprobante.UUID = timbre&.UUID
      obj.Comprobante.SelloCFD = timbre&.SelloCFD

      obj
    end
  end

  def xml_doc
    xml = raw_xml.is_a?(ActionDispatch::Http::UploadedFile) ? raw_xml.read : raw_xml
    @xml_doc ||= Nokogiri::XML(xml)
  end

  def factura_json
    return raw_json unless raw_json.respond_to?(:to_json)
    JSON.parse(raw_json.is_a?(String) ? raw_json : raw_json.to_json, object_class: OpenStruct) rescue nil
  end

  def raw_json
    @params[:json] || '{}'
  end

  def raw_xml
    @params[:xml]
  end

  def template_code
    @params[:code] || 'nbasic'
  end

  def pdf_template
    pdf_template_path = Rails.root.join('app', 'views', 'exportables', 'templates', "#{template_code}.pdf")
    File.exists?(pdf_template_path) ? pdf_template_path.to_s : nil
  end

  def view
    Rails.root.join('app', 'views', 'exportables', "#{template_code}.prawn")
  end

  def document_properties
    {
      template: pdf_template,
      margin: 0,
      skip_page_creation: true
    }
  end
end