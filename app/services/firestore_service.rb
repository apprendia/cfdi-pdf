require "google/cloud/firestore"

class FirestoreService
  attr_reader :firestore

  def initialize
    @firestore = Google::Cloud::Firestore.new(
      project_id: Rails.application.secrets.firebase[:project_id],
      credentials: Rails.application.secrets.firebase[:service_account]
    )
  end

  def get_factura(uuid)
    Rails.cache.fetch(uuid) do
      snapshot = firestore.doc("cfdis/#{uuid}").get
      snapshot.exists? ? snapshot.data : nil
    end
  end
end