class RealtimeDbService < Firebase::Client
  class << self

    def new
      super(Rails.application.secrets.firebase[:base_uri], private_key_json_string)
    end

    def private_key_json_string
      Rails.application.secrets.firebase[:service_account].to_json
    end
  end

  def get_body(path, query={})
    Rails.cache.fetch(path) do
      get(path, query={}).body
    end
  end
end

