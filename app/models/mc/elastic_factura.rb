module MC
  class ElasticFactura < OpenStruct

    class << self
      def find(uuid)
        uri = URI("#{Rails.application.config.elastic_host}/facturas/_search?q=uuid.keyword:#{uuid}&size=1&filter_path=hits.hits._source")
        response = nil

        Net::HTTP.start(uri.host, uri.port, use_ssl: uri.scheme == 'https') do |http|
          req = Net::HTTP::Get.new (uri)
          req.basic_auth 'elastic', 'elastic'
          response = http.request(req).body
        end

        Rails.cache.fetch ['mc-facturas', uuid] do
          new(JSON.parse(response)['hits']['hits'][0]['_source']) rescue nil
        end

      end
    end

    def template
      @template ||= Template.find_by(account_id: account, id: templateId) if templateId.present?
    end

  end
end