module MC
  class Template < OpenStruct

    class << self
      def find_by(account_id: , id: )
        body = RealtimeDbService.new.get_body("accounts/#{account_id}/facturaTemplates/#{id}")
        JSON.parse(body.to_json, object_class: self) if body.present?
      end
    end

  end
end