module PdfGeneratorHelper
  # extend ActiveSupport::Concern

  def fonts_dir
    Rails.root.join('app', 'assets', 'fonts')
  end

  def number_to_currency(number)
    ActionController::Base.helpers.number_to_currency(number).gsub('.00', '')
  end

  def number_with_delimiter(number, options = {})
    ActionController::Base.helpers.number_with_delimiter(number, options)
  end

  def number_to_percentage(number, options = {})
    ActionController::Base.helpers.number_to_percentage(number, options)
  end

  def nombre_catalogo(catalogo, clave)
      Sat.nombre_catalogo(catalogo, clave)
  end

  def nombre_impuesto(clave)
    Sat.nombre_impuesto(clave)
  end

  def conceptos_array(conceptos_node)
    node_array(conceptos_node)
  end

  def node_array(node)
    Array(node.instance_variable_get('@table').values).flatten
  end

  def cadena_original_tfd(xml_doc)
    tfd = xml_doc.xpath('//tfd:TimbreFiscalDigital', 'tfd' => 'http://www.sat.gob.mx/TimbreFiscalDigital')
    xslt = Nokogiri::XSLT(Rails.root.join('config', 'zfactu', 'cadenaoriginal_TFD_1_1.xslt').read)
    "#{xslt.transform(Nokogiri::XML(tfd.to_xml)).content.to_s.gsub('-', Prawn::Text::SHY)}"
  end

  def qr_text(factura_xml)
    "https://verificacfdi.facturaelectronica.sat.gob.mx/default.aspx?id=#{factura_xml.UUID}&re=#{factura_xml.Emisor.Rfc}&rr=#{factura_xml.Receptor.Rfc}&tt=#{factura_xml.Total}&fe=#{factura_xml.SelloCFD.to_s[-8, 8]}"
  end

  def qr_image_str(text)
    StringIO.new RQRCode::QRCode.new(text).as_png(module_px_size: 1).to_s
  end

  def complementos?(factura_xml)
    true
  end

  def moneda_txt(factura_xml)
    str = "#{factura_xml.Moneda} #{nombre_catalogo(:c_Moneda, factura_xml.Moneda)}"

    unless factura_xml.Moneda == 'MXN'
      str << " (tipo de cambio #{number_to_currency(factura_xml[:TipoCambio].to_f)} MXN) "
    end

    str
  end

  def concepto_txt(concepto)
    str = "#{concepto.Descripcion}"

    str = "(#{concepto.NoIdentificacion}) #{str}" if concepto.NoIdentificacion.present?
    str << "\nCuenta predial: #{concepto.CuentaPredial&.Numero}" if concepto.CuentaPredial.present?
    str << "\n<color rgb='666666'>Clave: #{concepto.ClaveProdServ}-#{nombre_catalogo :c_claveprodserv, concepto.ClaveProdServ}. Unidad: #{concepto.Unidad ? concepto.Unidad : concepto.ClaveUnidad}-#{nombre_catalogo(:c_claveunidad, concepto.ClaveUnidad) unless concepto.Unidad}</color>"

    str
  end

end