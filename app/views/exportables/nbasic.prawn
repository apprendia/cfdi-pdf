require "prawn/measurement_extensions"
extend PdfGeneratorHelper

font_size 7

font_families.update("Gotham Book" => {
  normal: fonts_dir.join('Gotham-Book.ttf').to_s
})
font_families.update("Gotham Medium" => {
  normal: fonts_dir.join('Gotham-Medium.ttf').to_s
})
font_families.update("Gotham Light" => {
  normal: fonts_dir.join('Gotham-Light.ttf').to_s
})
font_families.update("Gotham Bold" => {
  normal: fonts_dir.join('Gotham-Bold.ttf').to_s
})
font_families.update("Gotham Black" => {
  normal: fonts_dir.join('Gotham-Black.ttf').to_s
})

start_new_page(margin: 1.cm)


# Logo
if @factura_json&.template&.logo
  image_cell = make_cell({ image: open(@factura_json&.template&.logo), image_height: 2.2.cm, position: :left, padding_bottom: 0, padding_top: 0 })
  table([[image_cell]], width: bounds.width, cell_style: {padding: 0}) do
    rows(0..-1).borders = []
  end
  move_down 10
end

emisor_cell = make_cell(content: "#{@factura_xml.Emisor.Nombre&.to_s}
#{@factura_xml.Emisor.Rfc} | #{@factura_xml.Emisor.RegimenFiscal} #{nombre_catalogo(:c_RegimenFiscal, @factura_xml.Emisor.RegimenFiscal)}
#{[@factura_json&.emisor&.fiscalDirecciones&.first&.direccion, 'CP', @factura_json&.emisor&.fiscalDirecciones&.first&.cp].join(' ') if @factura_json&.emisor&.fiscalDirecciones&.first}
Código postal del lugar de expedición: #{@factura_xml.LugarExpedicion}", leading: 1.5.pt)

factura_cell = make_cell(content: "Folio fiscal: #{@factura_xml.UUID}
Fecha de emisión: #{I18n.l Time.zone.parse(@factura_xml.Fecha), format: :default }
Uso del Cfdi: #{@factura_xml.Receptor.UsoCFDI} #{nombre_catalogo :c_UsoCFDI, @factura_xml.Receptor.UsoCFDI}
Serie y Folio: #{@factura_json&.serie || @factura_xml.Serie} #{@factura_json&.folio || @factura_xml.Folio}", leading: 1.5.pt)

emisor_factura_table = table([['Emisor', 'Factura'], [emisor_cell, factura_cell]], { column_widths: { 1 => 220 } }) do
  row(0).font = 'Gotham Bold'
  row(0).size = 8.pt
  row(0).borders = []#[:top, :right, :left]
  row(1).borders = []#[:bottom, :right, :left]
  row(0..-1).border_width = 0.35
  row(0..-1).border_colors = '999999'
end

move_down 4


receptor_cell = make_cell(content: "#{@factura_xml.Receptor.Rfc} | #{@factura_xml.Receptor.Nombre&.to_s}
#{[@factura_json&.receptor&.fiscalDirecciones&.first&.direccion, 'CP', @factura_json&.receptor&.fiscalDirecciones&.first&.cp].join(' ') if @factura_json&.receptor&.fiscalDirecciones&.first}",
  leading: 1.5.pt)

pago_cell = make_cell(content: "Método: #{@factura_xml.MetodoPago} #{nombre_catalogo :c_MetodoPago, @factura_xml.MetodoPago}
Forma: #{@factura_xml.FormaPago} #{nombre_catalogo :c_FormaPago, @factura_xml.FormaPago.to_i}
Moneda: #{moneda_txt(@factura_xml)}
#{'Condiciones de pago: ' << @factura_xml.CondicionesDePago if @factura_xml.CondicionesDePago.present?}", leading: 1.5.pt)

receptor_pago_table = table([['Receptor', 'Pago'], [receptor_cell, pago_cell]], { width: bounds.width, column_widths: { 1 => 220 } }) do
  row(0).font = 'Gotham Bold'
  row(0).size = 8.pt

  row(0).borders = []#[:top, :right, :left]
  row(1).borders = []#[:bottom, :right, :left]
  row(0..-1).border_width = 0.35
  row(0..-1).border_colors = '999999'
end

move_down 4

if complementos?(@factura_xml)
  complementos = []

  ine = Basic::Complementos::Ine.render(self)
  complementos << [ine] if ine.present?

  cfdis_relacionados = Basic::Complementos::CfdisRelacionados.render(self)
  complementos << [cfdis_relacionados] if cfdis_relacionados.present?

  if complementos.any?
    complementos_table = table(complementos, width: bounds.width) do
      row(0..-1).border_width = 0#0.35
      row(0..-1).border_colors = '999999'
    end
  end
end

move_down 4

conceptos_cells = []
conceptos_count = Array(@factura_xml.Conceptos.Concepto).length
Array(@factura_xml.Conceptos.Concepto).each.with_index do |concepto, i|
  is_last = i == (conceptos_count - 1)
  is_first = i == 0
  concepto_padding = [(is_first ? 5 : 10), 5, (is_last ? 5 : 0)]

  conceptos_cells << [
    {
      content: concepto_txt(concepto),
      padding: concepto_padding,
      valign: :top,
      inline_format: true
    },
    { content: "#{number_with_delimiter concepto.Cantidad}", padding: concepto_padding, valign: :top },
    { content: "#{number_to_currency concepto.ValorUnitario}", padding: concepto_padding, valign: :top },
    { content: "#{concepto.Descuento ? number_to_currency(-concepto.Descuento.to_f) : '---' }", padding: concepto_padding, valign: :top },
    { content: "#{number_to_currency(concepto.Importe.to_f - concepto.Descuento.to_f)}", padding: concepto_padding, valign: :top }
  ]

  Array(concepto.Impuestos&.Traslados&.Traslado).each do |traslado|
    conceptos_cells << [
      { content: "Impuesto #{traslado.Impuesto} #{nombre_catalogo :c_impuesto, traslado.Impuesto.to_i} (Base #{number_to_currency traslado.Base})", padding: [0, 5, 0, 5], align: :right },
      { content: (traslado.TipoFactor == 'Exento' ? 'Exento' : "#{traslado.TasaOCuota.to_f * 100.0}%"), padding: [0, 5, 0, 5] },
      { content: "#{traslado.Importe ? number_to_currency(traslado.Importe) : ''}", padding: [0, 5, 0, 5] },
      '',
      { content: "", padding: [2, 5, 0, 5] }
    ]
  end

  Array(concepto.Impuestos&.Retenciones&.Retencion).each do |retencion|
    conceptos_cells << [
      { content: "Retención #{retencion.Impuesto} #{nombre_catalogo :c_impuesto, retencion.Impuesto.to_i} (Base #{number_to_currency retencion.Base})", padding: [0, 5, 0, 5], align: :right },
      { content: (retencion.TipoFactor == 'Exento' ? 'Exento' : "#{retencion.TasaOCuota.to_f * 100.0}%"), padding: [0, 5, 0, 5] },
      { content: "#{number_to_currency -retencion.Importe.to_f}", padding: [0, 5, 0, 5] },
      '',
      { content: "", padding: [2, 5, 0, 5] }
    ]
  end
end

impuestos_table = []
impuestos_locales = @factura_xml.Complemento&.ImpuestosLocales
if impuestos_locales.present?
  Array(impuestos_locales&.TrasladosLocales).each do |traslado|
    impuestos_table << [{ content: "#{traslado.ImpLocTrasladado} (#{number_to_percentage(traslado.TasadeTraslado)})", colspan: 4, align: :right }, "#{number_to_currency (traslado.Importe)}"]
  end

  Array(impuestos_locales&.RetencionesLocales).each do |retencion|
    impuestos_table << [{ content: "#{retencion.ImpLocRetenido} (#{number_to_percentage(retencion.TasadeRetencion)})", colspan: 4, align: :right }, "#{number_to_currency (-retencion.Importe.to_f)}"]
  end
end

totales_cell = [
  ['', '', '', { content: 'Subtotal' }, "#{number_to_currency (@factura_xml.SubTotal.to_f - @factura_xml.Descuento.to_f)}"],
  ['', '', '', { content: 'Impuestos' }, "#{number_to_currency (@factura_xml.Impuestos&.TotalImpuestosTrasladados.to_f - @factura_xml.Impuestos&.TotalImpuestosRetenidos.to_f)}"],
  *impuestos_table,
  ['', '', '', { content: 'Total', font_style: :bold, size: 8.pt }, { content: "#{number_to_currency @factura_xml.Total}", font_style: :bold, size: 8.pt }],
  [{ content: "#{@factura_xml.Total.to_f.humanize_divisa(@factura_xml.Moneda)}", colspan: 5 }]
].compact

conceptos_table = table(
  [
    [
      { content: 'Concepto' },
      { content: 'Cantidad' },
      { content: 'V. Unitario' },
      { content: 'Descuento' },
      { content: 'Importe' }
    ],
    *conceptos_cells,
    *totales_cell
  ],
  {
    header: true,
    width: bounds.width,
    column_widths: { 0 => 11.09.cm, 1 => 2.cm, 2 => 2.cm, 3 => 2.cm, 4 => 2.5.cm }
  }
) do
  row(0).font = 'Gotham Bold'
  row(0).size = 8.pt
  row(0).padding = [5, 5, 0, 5]
  columns(1..-1).align = :right
  row(-1).align = :right


  rows(0..-1).borders = []
  row(0).borders = [:top]
  column(0).borders = [:left]
  column(-1).borders = [:right]


  row(0).column(0).borders = [:top, :left]
  row(0).column(-1).borders = [:top, :right]
  row(-1).borders = [:bottom, :right, :left]

  # totales_rows
  (-totales_cell.length..-1).each do |i|
    row(i).padding = [i == totales_cell.length ? 10 : 3, 5, 3, 5]
  end

  rows(0..-1).border_width = 0.35
  rows(0..-1).border_colors = '999999'
end

if @factura_xml.Complemento&.TimbreFiscalDigital
  move_down 8
  sellos_table = table([["No. Certificado: #{@factura_xml.NoCertificado}. RFC Proveedor de Certificación: #{@factura_xml.Complemento.TimbreFiscalDigital.RfcProvCertif}.\nNo. Certificado SAT: #{@factura_xml.Complemento.TimbreFiscalDigital.NoCertificadoSAT}. Fecha de Certificación: #{@factura_xml.Complemento.TimbreFiscalDigital.FechaTimbrado}
  Cadena Original: #{cadena_original_tfd(@xml_doc)}
  Sello digital: #{@factura_xml.Complemento.TimbreFiscalDigital.SelloCFD}
  Sello del SAT: #{@factura_xml.Complemento.TimbreFiscalDigital.SelloSAT}",
    { image: qr_image_str(qr_text(@factura_xml)), fit: [3.cm, 3.cm], position: :right }]],
    { width: bounds.width, column_widths: { 1 => 3.cm } }) do
    columns(0).size = 6.pt
    rows(0..-1).borders = []
    rows(0..-1).padding = 0
  end
end

move_down 4
font_size 6
number_pages "Este documento es una representación impresa de un CFDI -- Página <page> de <total>", at: [bounds.bottom, bounds.left], align: :center