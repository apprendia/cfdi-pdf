require "prawn/measurement_extensions"
require "open-uri"
require 'net/http'
require 'json'
#require "prawn/fast_png"
require 'rqrcode'

fonts_dir = Rails.root.join('app', 'assets', 'fonts')

font_families.update("Gotham Book" => {
	:normal => fonts_dir.join('Gotham-Book.ttf').to_s
})
font_families.update("Gotham Medium" => {
	:normal => fonts_dir.join('Gotham-Medium.ttf').to_s
})
font_families.update("Gotham Light" => {
	:normal => fonts_dir.join('Gotham-Light.ttf').to_s
})
font_families.update("Gotham Bold" => {
	:normal => fonts_dir.join('Gotham-Bold.ttf').to_s
})
font_families.update("Gotham Black" => {
	:normal => fonts_dir.join('Gotham-Black.ttf').to_s
})

xml_doc ||= Nokogiri::XML(@xml)
tfd = xml_doc.xpath('//tfd:TimbreFiscalDigital', 'tfd' => 'http://www.sat.gob.mx/TimbreFiscalDigital')
xslt = Nokogiri::XSLT( Rails.root.join('config', 'zfactu', 'cadenaoriginal_TFD_1_1.xslt').read )
cadena_original = "#{xslt.transform(Nokogiri::XML(tfd.to_xml)).content.to_s.gsub('-', Prawn::Text::SHY)}"

def number_to_currency(number)
	ActionController::Base.helpers.number_to_currency(number).gsub('.00', '')
end

def nombre_catalogo(catalogo, clave)
	uri = URI("#{Rails.application.config.elastic_host}/#{catalogo.to_s.downcase}/_doc/#{clave}/_source?_source_include=name")
	response = Net::HTTP.get(uri)
	JSON.parse(response)['name'] rescue ''
end

def nombre_impuesto(clave)
	case clave
		when '001' then 'ISR'
		when '002' then 'IVA'
		when '003' then 'IEPS'
	end
end



start_new_page(:margin => 1.cm)
top = cursor
inner_page_width = bounds.width

font_size 8


text "<font size='10' name='Courier'><color rgb='ff0000'><b>#{@factura_xml.Serie}</b></color></font> <font>#{@factura_xml.Folio}</font>", inline_format: true, align: :right
text "<b>Folio fiscal</b>", inline_format: true, align: :right
text "#{@factura_json.uuid}", align: :right

if @factura_json&.template&.logo
	image open(@factura_json&.template&.logo), height: 3.cm, at: [0,top]
end

move_down 2.5.cm

bounding_box([0, cursor], width: inner_page_width) do
  half = (bounds.width / 2)
  
  bounding_box([0, bounds.top], :width => half) do
    text "<b>Emitido a</b>", inline_format: true
    text "#{@factura_xml.Receptor.Rfc}"
    text "#{@factura_xml.Receptor.Nombre}"
    text "#{@factura_json&.receptor&.fiscalDirecciones&.first&.direccion} CP #{@factura_json&.receptor&.fiscalDirecciones&.first&.cp}"
  end

  bounding_box([half, bounds.top], :width => half) do
    text "<b>Emitido por</b>", inline_format: true
    text "#{@factura_xml.Emisor.Rfc}"
    text "#{@factura_xml.Emisor.Nombre}"
    text "#{@factura_json&.emisor&.fiscalDirecciones&.first&.direccion} Código Porstal #{@factura_json&.emisor&.fiscalDirecciones&.first&.cp}"
    text "Regimen #{nombre_catalogo 'c_RegimenFiscal', @factura_xml.Emisor.RegimenFiscal}"
  end
end









@factura_json.recepcionPago&.pagos&.each_with_index do |pago,index|
	move_down 20
  half = (inner_page_width / 2)

  text "<font size='10'><b>Pago #{@factura_json.recepcionPago.pagos.length > 1 ? index + 1 : ''}</b></font>", inline_format: true
  move_down 10
  
  bounding_box([0, cursor], width: inner_page_width) do 
    bounding_box([0, bounds.top], :width => half) do
      text "Realizado el <b>#{I18n.l(Date.parse(pago.fechaPago), :format => :long)}</b> mediante <b>#{nombre_catalogo('c_FormaPago', pago.formaDePagoP)}</b>", inline_format: true
    end

    bounding_box([half, bounds.top], :width => half) do
      text "<font size='9'><b>Importe</b></font> <font size='9'>#{number_to_currency(pago.monto)} #{pago.monedaP}</font>", align: :right, inline_format: true
    end
  end


  # Datos banco
  if [pago.rfcEmisorCtaOrd, pago.nomBancoOrdExt, pago.ctaOrdenante, pago.rfcEmisorCuentaBen, pago.ctaBeneficiario, pago.numOperacion].any?
    move_down 15
    bounding_box([0, cursor], width: inner_page_width) do 
      bounding_box([0, bounds.top], :width => half) do
        text "RFC del banco ordenante: #{pago.rfcEmisorCtaOrd}", inline_format: true
        text "Nombre del Banco: #{pago.nomBancoOrdExt}", inline_format: true
        text "Cuenta ordenante: #{pago.ctaOrdenante}", inline_format: true

      end

      bounding_box([half, bounds.top], :width => half) do
        text "RFC del banco beneficiario: #{pago.rfcEmisorCuentaBen}", inline_format: true
        text "Cuenta del beneficiario: #{pago.ctaBeneficiario}", inline_format: true
        text "Número de Operación: #{pago.numOperacion}", inline_format: true
      end
    end
  end

  
  
  # UUIDs Relacionados
  move_down 10
  bounding_box([0, cursor], :width => inner_page_width) do
    header = [['Documento relacionado', 'Saldo anterior', 'Importe Pagado', 'Saldo restante']]

    body = pago.documentos.map{|cfdi|
      [
        "#{cfdi.idDocumento} (Parcialidad #{cfdi.numParcialidad})", number_to_currency(cfdi.impSaldoAnt), number_to_currency(cfdi.impPagado), number_to_currency(cfdi.impSaldoInsoluto)
      ]
    }

    table(header + body, {width: bounds.width}) do
      # Header style
      row(0).background_color = 'CCCCCC'
      row(0).font_style = :bold
      row(0).align = :center
      row(0).valign = :center
      row(0).borders = []
      row(0).padding = [0,0,4,0]
      
      row(1..-1).column(1..-1).align = :right
      row(1..-1).borders = [:bottom]
    end
  end






end








font_size 7
# Sellos
move_down 1.cm
position_qr = cursor

bounding_box([0.cm, cursor], width: inner_page_width) do
	table([
    [{content: "Tipo de comprobante: Pago"}],
    [{content: "Uso registrado en el CFDI: P01 Por definir"}],
    [{content: "Unidad de medida del CFDI: ACT"}],
    [{content: "Clave CFDI del concepto: 84111506"}],
    [{content: "Fecha de emisión #{I18n.l(Time.zone.parse(@factura_xml.Fecha))}"}],
    [{content: "Fecha de certificación #{I18n.l(Time.zone.parse(@factura_xml.Complemento.TimbreFiscalDigital.FechaTimbrado))}"}],
    [{content: "Certificado del emisor #{@factura_xml.NoCertificado}"}],
    [{content: "Certificado del SAT #{@factura_xml.Complemento.TimbreFiscalDigital.NoCertificadoSAT}"}],
    [{content: "RFC del Certificador #{@factura_xml.Complemento.TimbreFiscalDigital.RfcProvCertif}"}],
  ], {width: bounds.width, cell_style:{borders: [], padding: [1,1] }}) do
    
  end

  move_down 40
end

position_sellos = cursor

bounding_box([0.cm, position_sellos], width: inner_page_width) do
	table([
		[{content: 'Sello digital del CFDI', background_color: 'CCCCCC', font_style: :bold}],
		[@factura_xml.Sello],
		[{content: 'Sello del SAT', background_color: 'CCCCCC', font_style: :bold}],
		[@factura_xml.Complemento.TimbreFiscalDigital.SelloCFD],
		[{content: 'Cadena original del complemento de certificación del SAT', background_color: 'CCCCCC', font_style: :bold}],
		[cadena_original],
  ], {width: bounds.width, cell_style:{borders: [], padding: [1,1] }}) do
    
  end

  move_down 20

  # Leyendas oficiales
  text "Este documento es una representación impresa de un CFDI"
end

# Codigo QR
bounding_box([inner_page_width - 3.25.cm, position_qr], width: 3.5.cm) do
	tt = ActionController::Base.helpers.number_with_precision(@factura_xml.Total, :precision => 6)
	qr_txt = "https://verificacfdi.facturaelectronica.sat.gob.mx/default.aspx?id=#{@factura_xml.UUID}&re=#{@factura_xml.Emisor.Rfc}&rr=#{@factura_xml.Receptor.Rfc}&tt=#{@factura_xml.Total}&fe=#{@factura_xml.SelloCFD.to_s[-8,8]}"
	image StringIO.new(RQRCode::QRCode.new(qr_txt).as_png.to_s)
#	image open("http://chart.apis.google.com/chart?cht=qr&chs=#{300}x#{300}&chld=H%7C0&chl=#{CGI::escape(qr_txt)}"), :width => 3.25.cm, :height => 3.25.cm
end


if @factura_json.fechaCancelado
	create_stamp("stamp") do
	    fill_color "cc0000"
	    text_rendering_mode(:fill) do
				transparent(0.5){
					text_box "CANCELADO",
					:size   => 80,
					:width  => bounds.width,
					:height => bounds.height,
					:align  => :center,
					:valign => :center,
					:at     => [0, bounds.height],
					:rotate => 45,
					:rotate_around => :center
				}
	    end
	  end
	
	repeat (:all) do
	    stamp("stamp")
	end
end

if @factura_json.prueba
	create_stamp("stamp") do
	    fill_color "000000"
	    text_rendering_mode(:fill) do
				transparent(0.5){
					text_box "PRUEBA",
					:size   => 100,
					:width  => bounds.width,
					:height => bounds.height,
					:align  => :center,
					:valign => :center,
					:at     => [0, bounds.height],
					:rotate => 45,
					:rotate_around => :center
				}
	    end
	  end
	
	repeat (:all) do
	    stamp("stamp")
	end
end
