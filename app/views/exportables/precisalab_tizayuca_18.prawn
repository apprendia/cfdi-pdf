require "prawn/measurement_extensions"
require "open-uri"
require 'net/http'
require 'json'
#require "prawn/fast_png"
require 'rqrcode'


fonts_dir = Rails.root.join('app', 'assets', 'fonts')

font_families.update("Gotham Book" => {
	:normal => fonts_dir.join('Gotham-Book.ttf').to_s
})
font_families.update("Gotham Medium" => {
	:normal => fonts_dir.join('Gotham-Medium.ttf').to_s
})
font_families.update("Gotham Light" => {
	:normal => fonts_dir.join('Gotham-Light.ttf').to_s
})
font_families.update("Gotham Bold" => {
	:normal => fonts_dir.join('Gotham-Bold.ttf').to_s
})
font_families.update("Gotham Black" => {
	:normal => fonts_dir.join('Gotham-Black.ttf').to_s
})

xml_doc = Nokogiri::XML(@xml)
tfd = xml_doc.xpath('//tfd:TimbreFiscalDigital', 'tfd' => 'http://www.sat.gob.mx/TimbreFiscalDigital')
xslt = Nokogiri::XSLT( Rails.root.join('config', 'zfactu', 'cadenaoriginal_TFD_1_1.xslt').read )
cadena_original = "#{xslt.transform(Nokogiri::XML(tfd.to_xml)).content.to_s.gsub('-', Prawn::Text::SHY)}"

# puts "#{cadena_original}"

def number_to_currency(number)
	ActionController::Base.helpers.number_to_currency(number).gsub('.00', '')
end

def nombre_catalogo(catalogo, clave)
	uri = URI("#{Rails.application.config.elastic_host}/#{catalogo.to_s.downcase}/_doc/#{clave}/_source?_source_include=name")
	response = Net::HTTP.get(uri)
	JSON.parse(response)['name'] rescue ''
end

def nombre_impuesto(clave)
	case clave
		when '001' then 'ISR'
		when '002' then 'IVA'
		when '003' then 'IEPS'
	end
end

top = cursor

# ==== Logo del Emisor
# bounding_box([14.89.cm, top - 1.cm], :width => 5.54.cm, :height => 3.cm) do
# 	image(@factura_xml.fiscal_emisor.logo.path, :fit => [8.cm, 2.28.cm], :position => :center)
# end if @factura_xml.try(:fiscal_emisor).try(:logo) and File.exists?(@factura_xml.try(:fiscal_emisor).try(:logo).try(:path))

# ==== Fecha
formatted_text_box([
	{text: "#{I18n.l Time.parse(@factura_xml.Fecha), :format => "%d de %B del %Y a las %H:%M hrs"}", font: 'Gotham Bold' }
],{at: [14.89.cm, top-4.cm ], width: 5.54.cm, height: 0.57.cm, size: 7, leading: 1, align: :left})

# ==== Folio, Certificado, UUID
formatted_text_box([
	{text: "Folio: ", font: 'Gotham Light' },
	{text: "#{@factura_json.serie} #{@factura_json.folio}\n", font: 'Gotham Medium' },
	{text: "No. de certificado\n", font: 'Gotham Light' },
	{text: "#{@factura_xml.Complemento.TimbreFiscalDigital.NoCertificadoSAT}\n", font: 'Gotham Medium' },
	{text: "Folio fiscal SAT\n", font: 'Gotham Light' },
	{text: "#{@factura_xml.UUID}", font: 'Gotham Medium' }
],{at: [14.89.cm, top - 4.6.cm ], width: 5.54.cm, height: 2.08.cm, size: 7, leading: 1, align: :left})

# # ==== Datos del emisor
emisor_direccion = @factura_json.emisor.fiscalDirecciones[0]

formatted_text_box([
	{text: "Emisor\n", font: 'Gotham Bold', color: '7F7F7F' }
],{at: [14.89.cm, top-6.35.cm ], width: 5.54.cm, height: 0.3.cm,size: 7, leading: 1.1})

bounding_box([14.89.cm, top-6.65.cm ], width: 5.54.cm, height: 1.5.cm) do
	stroke_color 'FFFFFF'
	stroke_bounds
	formatted_text_box([
		{text: "#{@factura_xml.Emisor.Nombre}\n", font: 'Gotham Book' },
		{text: "#{@factura_xml.Emisor.Rfc}\n", font: 'Gotham Medium' },
		{text: "#{emisor_direccion ? emisor_direccion.direccion : ''} #{emisor_direccion ? emisor_direccion.cp : ''}\n", font: 'Gotham Book' },
		{text: "#{@factura_xml.Emisor.RegimenFiscal} #{nombre_catalogo 'c_RegimenFiscal', @factura_xml.Emisor.RegimenFiscal}\n", font: 'Gotham Book' },
	],{at: [bounds.left, bounds.top], width: bounds.width, height: bounds.width, overflow: :shrink_to_fit, size: 7, min_font_size: 6, leading: 1.1})
end

# # ==== Datos del receptor
formatted_text_box([
	{text: "Receptor\n", font: 'Gotham Bold', color: '7F7F7F' }
],{at: [14.89.cm, top-9.2.cm ], width: 5.64.cm, height: 0.3.cm,size: 7, leading: 1.1})


receptor_direccion = @factura_json.receptor.fiscalDirecciones[0]

bounding_box([14.89.cm, top-9.5.cm ], width: 5.54.cm, height: 2.cm) do
	stroke_color 'FFFFFF'
	stroke_bounds
	formatted_text_box([
		{text: "#{@factura_xml.Receptor.Nombre}\n", font: 'Gotham Book' },
		{text: "#{@factura_xml.Receptor.Rfc}\n", font: 'Gotham Medium' },
		{text: "#{receptor_direccion ? receptor_direccion.direccion : ''} #{receptor_direccion ? receptor_direccion.cp : ''}\n", font: 'Gotham Book' },
		{text: "#{@factura_xml.Receptor.UsoCFDI} #{nombre_catalogo 'c_UsoCFDI', @factura_xml.Receptor.UsoCFDI}", font: 'Gotham Light' }
	],{at: [bounds.left, bounds.top], width: bounds.width, height: bounds.width , overflow: :shrink_to_fit, size: 7, min_font_size: 6, leading: 1.1})
end

# # ==== Método de pago, lugar de expedición, observaciones, etc
factura_details = [
	{text: "Lugar de Expedición\n", font: 'Gotham Bold', color: '7F7F7F' },
	{text: "#{@factura_xml.LugarExpedicion.to_s}\n", font: 'Gotham Book' },
	{text: "Método de Pago\n", font: 'Gotham Bold', color: '7F7F7F' },
	{text: "#{@factura_xml.MetodoPago}\n", font: 'Gotham Book' },
	{text: "Forma de Pago\n", font: 'Gotham Bold', color: '7F7F7F' },
	{text: "#{@factura_xml.FormaPago} #{nombre_catalogo('c_FormaPago', @factura_xml.FormaPago.to_i)}\n", font: 'Gotham Book' },
]

bounding_box([14.89.cm, top-12.13.cm ], width: 5.54.cm, height: 3.cm) do
	stroke_color 'FFFFFF'
	stroke_bounds
	formatted_text_box(factura_details,{at: [bounds.left, bounds.top], width: bounds.width, height: bounds.width, size: 7, overflow: :shrink_to_fit, min_font_size: 6, leading: 1.4})
end

# # ==== Cellos digitales SAT
bounding_box([14.89.cm, top-15.8.cm ], width: 5.54.cm, height: 8.78.cm) do
	stroke_color 'FFFFFF'
	stroke_bounds

	formatted_text_box([
		{text: "Sello digital del CFDI:\n", font: 'Gotham Bold', color: '7F7F7F' },
		{text: "#{@factura_xml.Sello}\n", font: 'Gotham Light' },
		{text: "\nSello del SAT:\n", font: 'Gotham Bold', color: '7F7F7F' },
		{text: "#{@factura_xml.Complemento.TimbreFiscalDigital.SelloCFD}\n", font: 'Gotham Light' },
		{text: "\nCadena original del complemento de certificación digital del SAT:\n", font: 'Gotham Bold', color: '7F7F7F' },
		{text: "#{cadena_original} \n", font: 'Gotham Light' },
	],{at: [bounds.left, bounds.top], width: bounds.width, height: bounds.width , overflow: :shrink_to_fit, size: 7, min_font_size: 6, leading: 1, valign: :bottom})
end

# # ==== Codigo QR
tt = ActionController::Base.helpers.number_with_precision(@factura_xml.Total, :precision => 6)
qr_txt = "http://verificacfdi.facturaelectronica.sat.gob.mx/?re=#{@factura_xml.Emisor.Rfc}&rr=#{@factura_xml.Receptor.Rfc}&tt=#{tt}&id=#{@factura_xml.UUID}"
image StringIO.new(RQRCode::QRCode.new(qr_txt).as_png.to_s), :at => [14.89.cm, top-23.6.cm ], :width => 3.25.cm, :height => 3.25.cm
#image open("http://chart.apis.google.com/chart?cht=qr&chs=#{300}x#{300}&chld=H%7C0&chl=#{CGI::escape(qr_txt)}"), :at => [14.89.cm, top-23.6.cm ], :width => 3.25.cm, :height => 3.25.cm

# ==== LEYENDAS PRECISALAB
formatted_text_box([
	{text: "SERVICIO SUBROGADO DE ESTUDIOS DE LABORATORIO REALIZADOS A LA U.M.F. No. 18, DE ACUERDO A LA RELACION ANEXA DEL PERIODO DEL #{@factura_xml.CondicionesDePago || @factura_json.condicionesPago}\n", font: 'Gotham Light'},
	{text: "No. DE PARTIDA: 42062106\n", font: 'Gotham Book'},
	{text: "No. DE PROVEEDOR: 99386\n", font: 'Gotham Book'},
	{text: "No. DE CONTRATO: S8M0008\n", font: 'Gotham Book'},
	{text: "No. DE FIANZA: 3496-02685-3\n", font: 'Gotham Book'},
	{text: "AFIANZADORA: ASERTA, S.A. DE C.V.\n", font: 'Gotham Light'},
	{text: "FECHA DE EXPEDICION: 12 de enero 2018\n", font: 'Gotham Light'},
	{text: "MONTO $ 172,413.79\n", font: 'Gotham Light'}
],{at: [0.99.cm, top - 1.cm], :width => 13.34.cm, :height => 4.3.cm, size: 6, leading: 1.1, valign: :top})

# ==== FIRMAS PRECISALAB
# formatted_text_box([
# 	{text: "DRA. ALEJANDRA CITLALI CARMONA APARICIO\n", font: 'Gotham Book'},
# 	{text: "DIRECTORA U.M.F. NO.16", font: 'Gotham Light',size: 6}
# ],{at: [0.99.cm, top - 4.4.cm], :width => 6.57.cm, :height => 0.99.cm, size: 6, leading: 0.9, valign: :top})

formatted_text_box([
	{text: "DR. DEMETRIO CARLOS NAVARRO CORTEZ\n", font: 'Gotham Book'},
	{text: "DIRECTOR U.M.F. NO. 18\n", font: 'Gotham Light',size: 6}
],{at: [0.99.cm, top - 5.4.cm], :width => 6.57.cm, :height => 0.99.cm, size: 6, leading: 0.9, valign: :top})

formatted_text_box([
	{text: "DR. JESUS ANGEL QUINTERO RAMIREZ\n", font: 'Gotham Book'},
	{text: "TITULAR DE LA JEFATURA PRESTACIONES MEDICAS DEL IMSS", font: 'Gotham Light',size: 6}
],{at: [7.5.cm, top - 5.4.cm], :width => 6.57.cm, :height => 0.99.cm, size: 6, leading: 0.9, valign: :top})

formatted_text_box([
	{text: "DR. HECTOR AVILA RAMIREZ\n", font: 'Gotham Book'},
	{text: "JEFE DE SERVICIO DE LA U.M.F. NO. 18\n", font: 'Gotham Light',size: 6}
],{at: [0.99.cm, top - 8.2.cm], :width => 6.57.cm, :height => 0.99.cm, size: 6, leading: 0.9, valign: :top})

formatted_text_box([
	{text: "LIC. GEORGINA ALEXIS LUNA AMADOR\n", font: 'Gotham Book'},
	{text: "ADMINISTRADORA DE LA U.M.F. NO. 18", font: 'Gotham Light',size: 6}
],{at: [7.5.cm, top - 8.2.cm], :width => 6.57.cm, :height => 0.99.cm, size: 6, leading: 0.9, valign: :top})

# # ==== CONCEPTOS
bounding_box([0.99.cm, top - 9.47.cm], :width => 13.34.cm, :height => 16.31.cm) do
	conceptos = Array(@factura_xml.Conceptos.Concepto)
	table(conceptos.map{ |concepto|
		descripcion = "<font name='Gotham Light' size='7.5'>#{concepto.ClaveProdServ} #{concepto.Descripcion}</font>"
		unless concepto.CuentaPredial.to_s.empty?
			descripcion += "<font name='Gotham Bold' size='7'>\n\nCuenta Predial: </font><font name='Gotham Light' size='7'>#{concepto.CuentaPredial}</font>"
		end

		[	"<font name='Gotham Book' size='7.5'>#{concepto.Cantidad.to_s.gsub(/\.0+$/, '')}</font>",
			"<font name='Gotham Book' size='7.5'>#{concepto.ClaveUnidad.to_s} </font>",
			descripcion,
			"<font name='Gotham Book' size='7.5'>#{number_to_currency(concepto.ValorUnitario)}</font>",
			"<font name='Gotham Book' size='7.5'>#{number_to_currency(concepto.Importe)}</font>" ]},
		:width => 13.34.cm, :cell_style => { :inline_format => true, :border_width => 0, :padding => [0, 5, 1, 5] }) do
		column(0..1).style :align => :center
		column(3..4).style :align => :right
		column(0).width = 1.67.cm
		column(1).width = 1.77.cm
		column(3).width = 2.05.cm
		column(4).width = 2.5.cm
	end
end



bounding_box([0.99.cm, top - 19.81.cm], :width => 13.34.cm, :height => 6.31.cm) do
	conceptos = Array(@factura_xml.Conceptos.Concepto)

	text "<font name='Gotham Bold' size='7'>Descripción de Claves</font>\n", :align => :left, :valign => :top, :inline_format => true

	conceptos.group_by(&:ClaveUnidad).each do |key,values|
		if key
			text "<font name='Gotham Bold' size='7'>#{key}:</font> <font name='Gotham Light' size='7'>#{nombre_catalogo('c_ClaveUnidad', key)}</font>", :align => :left, :valign => :top, :inline_format => true, :leading => 0
		end
	end

	conceptos.group_by(&:ClaveProdServ).each do |key,values|
		if key
			text "<font name='Gotham Bold' size='7'>#{key}:</font> <font name='Gotham Light' size='7'>#{nombre_catalogo('c_ClaveProdServ', key)}</font>", :align => :left, :valign => :top, :inline_format => true, :leading => 0
		end
	end
end




# # ==== IMPUESTOS DESGLOSE
bounding_box([4.5.cm, top - 22.61.cm], :width => 4.34.cm, :height => 3.35.cm) do
	str = ""
	Array(@factura_xml.Impuestos.Traslados.Traslado).each{ |impuesto|
		#next unless impuesto.traslado?
		str += "<font name='Gotham Bold' size='7'>#{impuesto.Impuesto} #{nombre_impuesto(impuesto.Impuesto)} (#{(impuesto.TasaOCuota.to_f*100).round(2)}%): </font><font name='Gotham Light' size='7'>#{number_to_currency(impuesto.Importe.to_f.round(6))}\n\n</font>"
	}
	str = "<font name='Gotham Bold' size='7'>#{'-'*20}</font>" if str.empty?

	text str, :align => :left, :valign => :top, :inline_format => true
end if @factura_xml.Impuestos and @factura_xml.Impuestos.Traslados

bounding_box([1.1.cm, top - 22.61.cm], :width => 4.34.cm, :height => 3.35.cm) do
	str = ""
	Array(@factura_xml.Impuestos.Retenciones.Retencion).each{ |impuesto|
		#next unless impuesto.retencion?
		str += "<font name='Gotham Bold' size='7'>#{impuesto.Impuesto}: </font><font name='Gotham Light' size='7'>#{number_to_currency(impuesto.Importe.to_f.round(6))}\n\n</font>"
	}
	str = "<font name='Gotham Bold' size='7'>#{'-'*20}</font>" if str.empty?
	text str, :align => :left, :valign => :top, :inline_format => true
end if @factura_xml.Impuestos and @factura_xml.Impuestos.Retenciones

# # ==== TOTALES
bounding_box([11.80.cm, top - 22.2.cm], :width => 2.5.cm, :height => 0.91.cm) do
	text 	"<font name='Gotham Book' size='8'>#{number_to_currency(@factura_xml.SubTotal.to_f.round(6))}</font>", :align => :right, :valign => :center, :inline_format => true
end
bounding_box([11.80.cm, top - 23.01.cm], :width => 2.5.cm, :height => 0.7.cm) do
	text 	"<font name='Gotham Book' size='8'>#{number_to_currency(@factura_xml.Descuento.to_f.round(6))}</font>", :align => :right, :valign => :center, :inline_format => true
end
bounding_box([11.80.cm, top - 23.71.cm], :width => 2.5.cm, :height => 0.76.cm) do
	text 	"<font name='Gotham Book' size='8'>#{number_to_currency(@factura_xml.Impuestos.TotalImpuestosTrasladados.to_f.round(6))}</font>", :align => :right, :valign => :center, :inline_format => true
end if @factura_xml.Impuestos and @factura_xml.Impuestos.TotalImpuestosTrasladados
bounding_box([11.80.cm, top - 24.47.cm], :width => 2.5.cm, :height => 0.76.cm) do
	text 	"<font name='Gotham Book' size='8'>#{number_to_currency(@factura_xml.Impuestos.TotalImpuestosRetenidos.to_f.round(6))}</font>", :align => :right, :valign => :center, :inline_format => true
end if @factura_xml.Impuestos and @factura_xml.Impuestos.TotalImpuestosRetenidos
bounding_box([11.80.cm, top - 25.09.cm], :width => 2.5.cm, :height => 0.67.cm) do
	text 	"<font name='Gotham Black' size='8.5'>#{number_to_currency(@factura_xml.Total.to_f.round(6))}</font>", :align => :right, :valign => :center, :inline_format => true
end

# # ==== Total con letra
# bounding_box([0.99.cm, top - 25.76.cm], :width => 13.34.cm, :height => 0.45.cm) do
# 	text 	"<font name='Gotham Light' size='8'>#{@factura_xml.total.round(2).humanize_pesos.capitalize}</font>",
# 				:align => :right, :valign => :center, :inline_format => true
# end

# # ==== Watermark de PRUEBA
# if @factura_xml.test_only?
# 	transparent(0.10) do
# 		rotate(54, :origin => [9.77.cm, top - 12.94.cm]) do
# 			text_box('PRUEBA', :at => [-3.92.cm, top - 9.91.cm], :width => 29.39.cm, :height => 8.08.cm, :align => :center, :valign => :center, :size => 180)
# 		end
# 	end
# end

