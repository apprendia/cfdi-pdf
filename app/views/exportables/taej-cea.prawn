
require "prawn/measurement_extensions"
require "open-uri"
require 'net/http'
require 'json'
#require "prawn/fast_png"
require 'rqrcode'

font_size 7

xml_doc = Nokogiri::XML(@xml)
tfd = xml_doc.xpath('//tfd:TimbreFiscalDigital', 'tfd' => 'http://www.sat.gob.mx/TimbreFiscalDigital')
xslt = Nokogiri::XSLT( Rails.root.join('config', 'zfactu', 'cadenaoriginal_TFD_1_1.xslt').read )
cadena_original = "#{xslt.transform(Nokogiri::XML(tfd.to_xml)).content.to_s.gsub('-', Prawn::Text::SHY)}"

def number_to_currency(number)
	ActionController::Base.helpers.number_to_currency(number)#.gsub('.00', '')
end

def nombre_catalogo(catalogo, clave)
	uri = URI("#{Rails.application.config.elastic_host}/#{catalogo.to_s.downcase}/_doc/#{clave}/_source?_source_include=name")
	response = Net::HTTP.get(uri)
	JSON.parse(response)['name'] rescue ''
end

def nombre_impuesto(clave)
	case clave
		when '001' then 'ISR'
		when '002' then 'IVA'
		when '003' then 'IEPS'
	end
end


start_new_page(:margin => 1.cm)
top = cursor
inner_page_width = bounds.width


# Pre header
# Folio y serie
# Logo y emisor datos
emisor_datos = %{#{@factura_xml.Emisor.Rfc}
	#{@factura_xml.Emisor.Nombre}
	#{@factura_json&.emisor&.fiscalDirecciones&.first&.direccion} CP #{@factura_json&.emisor&.fiscalDirecciones&.first&.cp}
	Regimen #{nombre_catalogo 'c_RegimenFiscal', @factura_json&.emisor&.regimen}
}
table([
	[{content: "Factura <color rgb='ff0000'><b>#{@factura_json.folio} #{@factura_json.serie}</b></color>", colspan: 2, inline_format: true, align: :right, padding_top: 0, padding_bottom: 0}],
	[{image: open(@factura_json&.template&.logo), image_width: 4.cm, position: :center, width: 4.25.cm, padding_bottom: 0, padding_top: 0}, {content: emisor_datos, font_style: :bold, background_color: 'F8CE0B'}]
],{width: inner_page_width, position: 0.cm, cell_style: { borders: [] }})





# Header
# Receptor datos
# Características de factura
move_down 0.5.cm
table([
	[{content: 'Razón social',width: 3.cm,font_style: :bold}, {content: @factura_xml.Receptor.Nombre}, {content: 'RFC', font_style: :bold,width: 3.cm}, {content: @factura_xml.Receptor.Rfc}],
	[{content: 'Domicilio',width: 3.cm,font_style: :bold}, {content: "#{@factura_json&.receptor&.fiscalDirecciones&.first&.direccion} #{@factura_json&.receptor&.fiscalDirecciones&.first&.cp}", colspan: 3}],
	[{content: 'Uso del CFDI',width: 3.cm,font_style: :bold}, {content: "#{@factura_xml.Receptor.UsoCFDI} #{nombre_catalogo 'c_UsoCFDI', @factura_xml.Receptor.UsoCFDI}", colspan: 3}],
],{width: inner_page_width, cell_style: {padding_top: 0, padding_bottom: 0, borders: []}})

move_down 0.2.cm
table([
	[{content: 'Folio fiscal',width: 3.cm,font_style: :bold}, {content: @factura_xml.UUID}, {content: 'Fecha emisión', font_style: :bold,width: 3.cm}, {content: I18n.l(Time.zone.parse(@factura_xml.Fecha))}],
	[{content: 'Certificado emisor',width: 3.cm,font_style: :bold}, {content: @factura_xml.NoCertificado}, {content: 'Fecha certificación', font_style: :bold,width: 3.cm}, {content: I18n.l(Time.zone.parse(@factura_xml.Complemento.TimbreFiscalDigital.FechaTimbrado))}],
	[{content: 'Certificado SAT',width: 3.cm,font_style: :bold}, {content: @factura_xml.Complemento.TimbreFiscalDigital.NoCertificadoSAT}, {content: 'RFC Certificador', font_style: :bold,width: 3.cm}, {content: @factura_xml.Complemento.TimbreFiscalDigital.RfcProvCertif}],
	[{content: 'Forma pago',width: 3.cm,font_style: :bold}, {content: "#{@factura_xml.FormaPago} #{nombre_catalogo('c_FormaPago',@factura_xml.FormaPago.to_i)}"}, {content: 'Método de pago', font_style: :bold,width: 3.cm}, {content: "#{@factura_xml.MetodoPago} #{nombre_catalogo('c_MetodoPago',@factura_xml.MetodoPago)}"}],
	[{content: 'Tipo de Comprobante',width: 3.cm,font_style: :bold}, {content: "#{@factura_xml.TipoDeComprobante} Ingreso"}, {content: 'Moneda', font_style: :bold,width: 3.cm}, {content: "#{@factura_xml.Moneda} #{nombre_catalogo('c_Moneda',@factura_xml.Moneda)}"}],
],{width: inner_page_width, cell_style: {padding_top: 0, padding_bottom: 0, borders: []}})





# Conceptos
move_down 0.5.cm

header = [
	{content: 'Cant', width: 1.5.cm},
	{content: 'Unidad', width: 1.5.cm},
	{content: 'Concepto'},
	{content: 'Valor Unitario', width: 2.5.cm},
	{content: 'Importe', width: 2.5.cm}
]


conceptos_cells = []

@factura_json&.conceptos&.each do |concepto|
	conceptos_cells << [
		concepto.cantidad,
		concepto.claveUnidad,
		"#{concepto.descripcion}. Clave: #{concepto.clave}-#{nombre_catalogo 'c_ClaveProdServ', concepto.clave}",
		number_to_currency(concepto.valorUnitario),
		number_to_currency(concepto.importe)
	]
end

table([
	header,
	*conceptos_cells
], {header: true, width: inner_page_width, position: 0.cm, cell_style: {borders: []}}) do
	row(0).background_color = 'F8CE0B'
	row(0).font_style = :bold
	row(0).align = :center

	row(0).padding = 0
	row(0..-1).padding_bottom = 0

	column(0..1).align = :center
	row(1..-1).column(3..4).align = :right
end



def pt2mm(pt)
      pt * 1 / mm2pt(1) # (25.4 / 72)
    end
    def mm2pt(mm)
      mm * (72 / 25.4)
    end
# Firma del representante
move_down 0.5.cm
formatted_text_box([
	{text: "Recibí\n", styles: [:bold]},
],{at: [0.cm, cursor ], width: inner_page_width, height: 2.5.cm, align: :center})


move_down 0.25.cm
formatted_text_box([
	{text: "\n\n"},
	{text: "_"*40},
	{text: "\nLic. Leonardo Perez Calva"},
	{text: "\nDirector General", styles: [:bold]}
],{at: [0.cm, cursor ], width: inner_page_width / 3, height: 2.5.cm, align: :center})

formatted_text_box([
	{text: "\n\n"},
	{text: "_"*40},
	{text: "\nIng. Antonio de Jesús Olvera Mota"},
	{text: "\nAdministrador Único", styles: [:bold]}
],{at: [(inner_page_width / 3), cursor ], width: inner_page_width / 3, height: 2.5.cm, align: :center})

formatted_text_box([
	{text: "\n\n"},
	{text: "_"*40},
	{text: "\n"},
	{text: "\nSupervisor de Obra", styles: [:bold]}
],{at: [(inner_page_width / 3 * 2), cursor ], width: inner_page_width / 3, height: 2.5.cm, align: :center})


# Lic. Leonardo Perez calva 
# Director general 
# Ing. Antonio de Jesús Olvera mota 
# Administrador único 
# Luis Alfonso Meneses Rodríguez 
# Supervisor de obra


# Totales
# Importe con letra
# Totales e impuestos
move_down 2.5.cm

total_position = cursor
gap = 0.25.cm

# Total con letra
bounding_box([0.cm, total_position], width: (inner_page_width / 2) - gap) do
	table([
		['Importe con letra'],
		["#{@factura_xml.Total.to_f.humanize_divisa(@factura_xml.Moneda)}"]
	],{width: bounds.width, cell_style: {borders: []}}) do
		row(0).background_color = 'F8CE0B'
		row(0).font_style = :bold
		row(0).align = :center
		row(0).padding = 0
	end
end

# Subtotal
# IVA calculado
# Retenciones
# Total
impuestos = Array(@factura_xml.Impuestos.Traslados.Traslado)

impuestos_row = (impuestos.map{|imp|
			imp.calculated_impuesto = imp.Importe.to_f# + (imp.TasaOCuota.to_f * @factura_xml.Descuento.to_f)
			[{content: ''}, {content: "#{nombre_impuesto(imp.Impuesto)} #{imp.TasaOCuota.to_f * 100}%"}, {content: number_to_currency(imp.calculated_impuesto)}]
		})

retenciones_locales = @factura_xml.Complemento.ImpuestosLocales ? Array(@factura_xml.Complemento.ImpuestosLocales.RetencionesLocales) : []
bounding_box([inner_page_width / 2 + gap, total_position], width: (inner_page_width / 2) - gap) do
	table([
		[{content: ''}, {content: 'Importe de estimación (sin IVA)', width: 5.cm, font_style: :bold}, {content: "#{number_to_currency(@factura_xml.SubTotal)}"}],
		[{content: ''}, {content: 'Amortización de anticipo'}, {content: "#{number_to_currency(@factura_xml.Descuento.to_f)}"}],
		[{content: ''}, {content: 'Subtotal', font_style: :bold}, {content: "#{number_to_currency(@factura_xml.SubTotal.to_f - @factura_xml.Descuento.to_f)}"}],
		*impuestos_row,
		[{content: ''}, {content: 'Total', font_style: :bold}, {content: "#{number_to_currency(@factura_xml.SubTotal.to_f + impuestos.map(&:calculated_impuesto).reduce(:+) - @factura_xml.Descuento.to_f)}"}],
		[{content: ''}, {content: 'Deducciones', font_style: :bold}, {content: ''}],
		*(retenciones_locales.map{|ret|
			[{content: ''}, {content: ret.ImpLocRetenido}, {content: number_to_currency(ret.Importe)}]
		}),
		[{content: ''}, {content: 'Total de deducciones', font_style: :bold}, {content: "#{number_to_currency(@factura_xml.Complemento.ImpuestosLocales ? @factura_xml.Complemento.ImpuestosLocales.TotaldeRetenciones : 0)}"}],
		[{content: ''}, {content: 'Importe líquido a pagar', font_style: :bold}, {content: "#{number_to_currency(@factura_xml.Total)}"}],
	], {width: bounds.width, cell_style:{padding_top: 0, borders: []}}) do
		column(1..2).align = :right
		column(2).width = 2.5.cm
	end
end





# Sellos
move_down 0.5.cm
position_sellos = cursor

sellos_box = bounding_box([0.cm, position_sellos], width: inner_page_width - 3.5.cm) do
	table([
		[{content: 'Sello digital del CFDI', background_color: 'F8CE0B', font_style: :bold}],
		[@factura_xml.Sello],
		[{content: 'Sello del SAT', background_color: 'F8CE0B', font_style: :bold}],
		[@factura_xml.Complemento.TimbreFiscalDigital.SelloCFD],
		[{content: 'Cadena original del complemento de certificación del SAT', background_color: 'F8CE0B', font_style: :bold}],
		[cadena_original],
	], {width: bounds.width, cell_style:{borders: []}}) do
		cells.style do |c|
			c.padding_top = 0 if (c.row % 2).zero?
			c.padding_bottom = 0 if (c.row % 2).zero?
		end
	end
end

# Codigo QR
bounding_box([inner_page_width - 3.25.cm, position_sellos], width: 3.5.cm) do
	tt = ActionController::Base.helpers.number_with_precision(@factura_xml.Total, :precision => 6)
	qr_txt = "https://verificacfdi.facturaelectronica.sat.gob.mx/default.aspx?id=#{@factura_xml.UUID}&re=#{@factura_xml.Emisor.Rfc}&rr=#{@factura_xml.Receptor.Rfc}&tt=#{@factura_xml.Total}&fe=#{@factura_xml.SelloCFD.to_s[-8,8]}"
	image StringIO.new(RQRCode::QRCode.new(qr_txt).as_png.to_s)
	#image open("http://chart.apis.google.com/chart?cht=qr&chs=#{300}x#{300}&chld=H%7C0&chl=#{CGI::escape(qr_txt)}"), :width => 3.25.cm, :height => 3.25.cm
end


# Leyendas oficiales
move_down 3.5.cm
text "Efectos fiscales al pago. Este documento es una representación impresa de un CFDI"


