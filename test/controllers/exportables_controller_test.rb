require 'test_helper'

class ExportablesControllerTest < ActionDispatch::IntegrationTest
  test "should get pdf" do
    get exportables_pdf_url
    assert_response :success
  end

end
